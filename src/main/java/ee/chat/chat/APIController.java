package ee.chat.chat;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@CrossOrigin
@RestController
public class APIController {

    public HashMap<String, ChatRoom> chatrooms = new HashMap();

    public APIController() {
        String defaultRoom = "general";
        ChatRoom chatroom = new ChatRoom(defaultRoom);
        chatroom.addMessage(new ChatMessage("admin", "Teretulemast!"));
        chatrooms.put(defaultRoom, chatroom);
    }

    @RequestMapping("/")
    String index() {
        return "proovi /chat/general";
    }

    @GetMapping("/chat/{room}")
    ChatRoom messages(@PathVariable String room) {
        return chatrooms.get(room);
    }

    @PostMapping("/chat/{room}/new-message")
    ChatMessage newMessage(@RequestBody ChatMessage message, @PathVariable String room) {
        ChatRoom chatroom = chatrooms.get(room);
        chatroom.addMessage(message);
        System.out.println(message);
        return message;
    }


}
