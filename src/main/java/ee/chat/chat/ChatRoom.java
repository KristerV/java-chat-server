package ee.chat.chat;

import java.util.ArrayList;

public class ChatRoom {

    private ArrayList<ChatMessage> messages = new ArrayList();
    private String room;

    public ChatRoom(String room) {
        this.room = room;
    }

    public void addMessage(ChatMessage msg) {
        messages.add(msg);
    }

    public ArrayList<ChatMessage> getMessages() {
        return messages;
    }

    public String getRoom() {
        return room;
    }
}