package ee.chat.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;
import java.util.Date;

public class ChatMessage {

    private String message;
    private String user;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date date;

    public ChatMessage() {}

    public ChatMessage(String user, String msg) {
        this.message = msg;
        this.user = user;
        this.date = new Date();
    }

    public String getMessage() {
        return message;
    }

    public String getUser() {
        return user;
    }

    public Date getDate() {
        return date;
    }
}
